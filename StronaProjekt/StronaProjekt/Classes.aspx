﻿<%@ Page Title="Strona z zajęciami" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Classes.aspx.cs" Inherits="StronaProjekt.Classes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .textboxwidth {
            max-width: 100px;
        }
    </style>


    <h2><%: Title %>.</h2>
    <h3><asp:Label ID="LoginLabel" runat="server" Text=""></asp:Label></h3>
    <br />
    <div class="jumbotron " runat="server" id="MyClassesDiv" Visible="False">

        <h1>Twoje zajęcia:</h1>
        <p class="lead">
                <asp:GridView ID="MyClassesGridView" runat="server" OnRowCommand="MyClassesGridView_RowCommand" class="table table-bordered table-condensed table-responsive table-hover ">
                 <Columns>
                    <asp:ButtonField ButtonType="Button" CommandName="ShowDetails" Text="Szczegóły" />
                </Columns>
                </asp:GridView>
        </p>

    </div>

    <div class="jumbotron " runat="server" id="TeacherDiv" Visible="False">

        <h1>Twoje zajęcia:</h1>
        <p class="lead">
                <asp:GridView ID="TeacherGridView" runat="server" OnRowCommand="TeacherGridView_RowCommand" class="table table-bordered table-condensed table-responsive table-hover ">
                 <Columns>
                    <asp:ButtonField ButtonType="Button" CommandName="EditDetails" Text="Edytuj" />
                </Columns>
                </asp:GridView>
        </p>

    </div>



    <div class="jumbotron" runat="server" id="ClassesDiv" Visible="False">
        <h1>Wszystkie zajęcia:</h1>
        <p class="lead">
            <asp:GridView ID="ClassesGridView" runat="server" class="table table-bordered table-condensed table-responsive table-hover "></asp:GridView>

        </p> 
    </div>
    <div class="jumbotron" runat="server" id="EnrollDiv" Visible="False">
        <h1>Zapisz się na zajęcia:</h1>
        <p class="lead">
            <asp:GridView ID="EnrollGridView" runat="server" OnRowCommand="EnrollGridView_RowCommand" class="table table-bordered table-condensed table-responsive table-hover ">
                <Columns>
                    <asp:ButtonField ButtonType="Button" CommandName="Enroll" Text="Zapisz" />
                </Columns>
            </asp:GridView>
        </p>
    </div>



 
    <asp:Button ID="AddClassesButton" runat="server" Text="Dodaj" OnClick="AddClassesButton_Click" Visible ="false"/>

</asp:Content>
