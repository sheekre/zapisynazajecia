﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StronaProjekt
{
    /// <summary>
    /// Dodawanie zajęć.
    /// 
    /// Klasa odpowiadająca za dodawanie zajęć przez prowadzącego.
    /// @param connectionString Zmienna do przechowywania Connection Stringa do bazy danych.
    /// </summary>
    public partial class AddClasses : System.Web.UI.Page
    {
        // Zmienna do przechowywania connectionString'a
        string connectionString = "";


        /// <summary>
        /// Załadowanie strony i wczytanie Connection Stringa. W przypadku gdy ktoś wejdzie z linku bez sesji następuje przeniesienia ns stronę logowania. 
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["login"] == null)
                Response.Redirect("Login.aspx");
            if (Session["permission"].ToString() == "3")
                Response.Redirect("Default.aspx");

            // Wczytanie connectionStringa
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnection"].ConnectionString;

            if (!IsPostBack)
            {
                loadData();
            }
        }


        /// <summary>
        /// Załadowanie danych (list rozwijanych).
        /// </summary>
        protected void loadData()
        {
            loadPrzedmioty();
            loadWydzial();
            loadKierunek();
            loadZastepstwo();
        }

        /// <summary>
        /// Załadowanie listy przedmiotów.
        /// </summary>
        protected void loadPrzedmioty()
        {
            var select = @"select Id,Nazwa from dbo.Przedmioty";
            var c = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(select, c);

            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds);
            var dt = ds.Tables[0];

            PrzedmiotyDDL.DataTextField = dt.Columns["Nazwa"].ToString();
            PrzedmiotyDDL.DataValueField = dt.Columns["Id"].ToString();

            PrzedmiotyDDL.DataSource = dt;
            PrzedmiotyDDL.DataBind();
        }


        /// <summary>
        /// Załadowanie listy wydziałów.
        /// </summary>
        protected void loadWydzial()
        {
            var select = @"select Id, CONCAT(Nazwa, ' (', Oznaczenie,  ')') AS 'Wydzial' from dbo.Wydzialy";
            var c = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(select, c);

            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds);
            var dt = ds.Tables[0];

            WydzialDDL.DataTextField = dt.Columns["Wydzial"].ToString();
            WydzialDDL.DataValueField = dt.Columns["Id"].ToString();

            WydzialDDL.DataSource = dt;
            WydzialDDL.DataBind();
        }


        /// <summary>
        /// Załadowanie listy kierunków.
        /// </summary>
        protected void loadKierunek()
        {
            var select = @"select Id, CONCAT(Nazwa, ' (', Oznaczenie,  ')') AS 'Kierunek' from dbo.Kierunki";
            var c = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(select, c);

            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds);
            var dt = ds.Tables[0];

            KierunekDDL.DataTextField = dt.Columns["Kierunek"].ToString();
            KierunekDDL.DataValueField = dt.Columns["Id"].ToString();

            KierunekDDL.DataSource = dt;
            KierunekDDL.DataBind();
        }


        /// <summary>
        /// Załadowanie listy pracowników do zastępstwa.
        /// </summary>
        protected void loadZastepstwo()
        {
            var select = @"select Id, CONCAT(Imie, ' ', Nazwisko) AS 'Nazwa' from dbo.Pracownicy
                           where IdProfilUzytkownika <> '" + Session["unique"] + "'";
            var c = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(select, c);

            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds);
            var dt = ds.Tables[0];

            ZastepstwoDDL.DataTextField = dt.Columns["Nazwa"].ToString();
            ZastepstwoDDL.DataValueField = dt.Columns["Id"].ToString();

            ZastepstwoDDL.DataSource = dt;
            ZastepstwoDDL.DataBind();
        }


        /// <summary>
        /// Obsługa checkboxa Ogólnouczelniane.
        /// </summary>
        protected void OgolnouczelnianeCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (OgolnouczelnianeCheckBox.Checked)
            {
                Wydzial.Visible = false;
                Kierunek.Visible = false;
            }
            else
            {
                Wydzial.Visible = true;
                Kierunek.Visible = true;
            }
        }


        /// <summary>
        /// Obsługa checkboxa Zdalne.
        /// </summary>
        protected void ZdalneCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ZdalneCheckBox.Checked)
                Sala.Visible = false;
            else
                Sala.Visible = true;
        }


        /// <summary>
        /// Obsługa checkboxa Zastępstwo.
        /// </summary>
        protected void ZastepstwoCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ZastepstwoCheckBox.Checked)
                Zastepstwo.Visible = true;
            else
                Zastepstwo.Visible = false;
        }


        /// <summary>
        /// Pobranie Id aktualnie zalogowanego pracownika.
        /// </summary>
        protected int returnPracownikId()
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();

                // Zapytanie SQL
                string query = @"select Id from dbo.Pracownicy
                            where IdProfilUzytkownika = '" + Session["unique"] + "'";

                SqlCommand sql = new SqlCommand(query, con);

                int result = Convert.ToInt32(sql.ExecuteScalar());
                con.Close();

                return result;
            }
        }


        /// <summary>
        /// Obsługa dodawania zajęcia.
        /// </summary>
        protected void AddButton_Click(object sender, EventArgs e)
        {
            //Jesli wszystkie pola zostaly wypelnione
            if (CalendarTextBox.Text.Length > 0 && RozpoczecieTextBox.Text.Length > 0 && ZakonczenieTextBox.Text.Length > 0 &&
                OpisTextBox.Text.Length > 0 && TematTextBox.Text.Length > 0)
            {
                // Jesli nie zdalne
                if (ZdalneCheckBox.Checked == false)
                {
                    if (SalaTextBox.Text.Length > 0)
                    {
                        using (SqlConnection con = new SqlConnection(connectionString))
                        {
                            con.Open();

                            // Zapytanie SQL
                            string query = @"INSERT INTO [dbo].[Zajecia]
                                ([IdPrzedmiot],[Data],[GodzinaRozpoczecia],[GodzinaZakonczenia],[Opis],[IdPracownikProwadzacy],[CzyOgolnoUczelniane],
                                [IdWydzial],[IdKierunek],[CzyZdalnie],[IdSala],[CzyOdwolane],[CzyZastpestwo],[IdPracownikZastepstwo],[Temat])
                                VALUES (@IdPrzedmiot, @Data, @GodzinaRozpoczecia, @GodzinaZakonczenia, @Opis, @IdPracownikProwadzacy, @CzyOgolnoUczelniane, 
                                @IdWydzial, @IdKierunek, @CzyZdalnie, @IdSala, 0, @CzyZastpestwo, @IdPracownikZastepstwo, @Temat)";

                            SqlCommand sqlCmd = new SqlCommand(query, con);
                            sqlCmd.Parameters.AddWithValue("@IdPrzedmiot", PrzedmiotyDDL.SelectedItem.Value);
                            sqlCmd.Parameters.AddWithValue("@Data", CalendarTextBox.Text);
                            sqlCmd.Parameters.AddWithValue("@GodzinaRozpoczecia", RozpoczecieTextBox.Text + ":00");
                            sqlCmd.Parameters.AddWithValue("@GodzinaZakonczenia", ZakonczenieTextBox.Text + ":00");
                            sqlCmd.Parameters.AddWithValue("@Opis", OpisTextBox.Text);
                            sqlCmd.Parameters.AddWithValue("@IdPracownikProwadzacy", returnPracownikId());
                            sqlCmd.Parameters.AddWithValue("@CzyOgolnoUczelniane", OgolnouczelnianeCheckBox.Checked);

                            if (OgolnouczelnianeCheckBox.Checked == false)
                            {
                                sqlCmd.Parameters.AddWithValue("@IdWydzial", WydzialDDL.SelectedItem.Value);
                                sqlCmd.Parameters.AddWithValue("@IdKierunek", KierunekDDL.SelectedItem.Value);
                            }
                            else if (OgolnouczelnianeCheckBox.Checked)
                            {
                                sqlCmd.Parameters.AddWithValue("@IdWydzial", DBNull.Value);
                                sqlCmd.Parameters.AddWithValue("@IdKierunek", DBNull.Value);
                            }

                            sqlCmd.Parameters.AddWithValue("@CzyZdalnie", ZdalneCheckBox.Checked);
                            sqlCmd.Parameters.AddWithValue("@IdSala", SalaTextBox.Text);

                            sqlCmd.Parameters.AddWithValue("@CzyZastpestwo", ZastepstwoCheckBox.Checked);
                            if (ZastepstwoCheckBox.Checked == false)
                                sqlCmd.Parameters.AddWithValue("@IdPracownikZastepstwo", DBNull.Value);
                            else if (ZastepstwoCheckBox.Checked)
                                sqlCmd.Parameters.AddWithValue("@IdPracownikZastepstwo", ZastepstwoDDL.SelectedItem.Value);

                            sqlCmd.Parameters.AddWithValue("@Temat", TematTextBox.Text);


                            sqlCmd.ExecuteScalar();
                            lblSuccessMessage.Text = "Dodano nowy rekord.";
                            lblErrorMessage.Text = "";
                            con.Close();

                        }
                    }
                    else
                    {
                        lblSuccessMessage.Text = "";
                        lblErrorMessage.Text = "Wpisz numer sali.";
                    }
                }
                else
                {
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();

                        // Zapytanie SQL
                        string query = @"INSERT INTO [dbo].[Zajecia]
                                ([IdPrzedmiot],[Data],[GodzinaRozpoczecia],[GodzinaZakonczenia],[Opis],[IdPracownikProwadzacy],[CzyOgolnoUczelniane],
                                [IdWydzial],[IdKierunek],[CzyZdalnie],[IdSala],[CzyOdwolane],[CzyZastpestwo],[IdPracownikZastepstwo],[Temat])
                                VALUES (@IdPrzedmiot, @Data, @GodzinaRozpoczecia, @GodzinaZakonczenia, @Opis, @IdPracownikProwadzacy, @CzyOgolnoUczelniane, 
                                @IdWydzial, @IdKierunek, @CzyZdalnie, @IdSala, 0, @CzyZastpestwo, @IdPracownikZastepstwo, @Temat)";

                        SqlCommand sqlCmd = new SqlCommand(query, con);
                        sqlCmd.Parameters.AddWithValue("@IdPrzedmiot", PrzedmiotyDDL.SelectedItem.Value);
                        sqlCmd.Parameters.AddWithValue("@Data", CalendarTextBox.Text);
                        sqlCmd.Parameters.AddWithValue("@GodzinaRozpoczecia", RozpoczecieTextBox.Text + ":00");
                        sqlCmd.Parameters.AddWithValue("@GodzinaZakonczenia", ZakonczenieTextBox.Text + ":00");
                        sqlCmd.Parameters.AddWithValue("@Opis", OpisTextBox.Text);
                        sqlCmd.Parameters.AddWithValue("@IdPracownikProwadzacy", returnPracownikId());
                        sqlCmd.Parameters.AddWithValue("@CzyOgolnoUczelniane", OgolnouczelnianeCheckBox.Checked);

                        if (OgolnouczelnianeCheckBox.Checked == false)
                        {
                            sqlCmd.Parameters.AddWithValue("@IdWydzial", WydzialDDL.SelectedItem.Value);
                            sqlCmd.Parameters.AddWithValue("@IdKierunek", KierunekDDL.SelectedItem.Value);
                        }
                        else if (OgolnouczelnianeCheckBox.Checked)
                        {
                            sqlCmd.Parameters.AddWithValue("@IdWydzial", DBNull.Value);
                            sqlCmd.Parameters.AddWithValue("@IdKierunek", DBNull.Value);
                        }

                        sqlCmd.Parameters.AddWithValue("@CzyZdalnie", ZdalneCheckBox.Checked);
                        sqlCmd.Parameters.AddWithValue("@IdSala", DBNull.Value);

                        sqlCmd.Parameters.AddWithValue("@CzyZastpestwo", ZastepstwoCheckBox.Checked);
                        if (ZastepstwoCheckBox.Checked == false)
                            sqlCmd.Parameters.AddWithValue("@IdPracownikZastepstwo", DBNull.Value);
                        else if (ZastepstwoCheckBox.Checked)
                            sqlCmd.Parameters.AddWithValue("@IdPracownikZastepstwo", ZastepstwoDDL.SelectedItem.Value);

                        sqlCmd.Parameters.AddWithValue("@Temat", TematTextBox.Text);

                        sqlCmd.ExecuteScalar();
                        lblSuccessMessage.Text = "Dodano nowy rekord.";
                        lblErrorMessage.Text = "";
                        con.Close();

                    }
                }
            }
            else
            {
                lblErrorMessage.Text = "Musisz wypełnić wszystkie pola.";
                lblSuccessMessage.Text = "";
            }
        }
    }
}