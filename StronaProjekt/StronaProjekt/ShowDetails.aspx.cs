﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StronaProjekt
{
    /// <summary>
    /// Wyświetlenie szczegółów zajęcia.
    /// 
    /// Klasa obsługująca wyświetlanie szczegółów zajęć po użyciu przycisku Szczegóły.
    /// @param connectionString Zmienna do przechowywania Connection Stringa do bazy danych.
    /// @param values Zmienna do przechowywania listy pobranych elementów z bazy danych.
    /// </summary>
    public partial class ShowDetails : System.Web.UI.Page
    {
        // Zmienna do przechowywania connectionString'a
        string connectionString = "";
        List<string> values = new List<string>();


        /// <summary>
        /// Wczytanie strony oraz Connection Stringa.
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["login"] == null)
                Response.Redirect("Login.aspx");

            // Wczytanie connectionStringa
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnection"].ConnectionString;
            values = (List<string>)Session["details"];

            if (!IsPostBack)
            {
                setValues();
            }
        }


        /// <summary>
        /// Ustawienie odpowiednich wartości w polach adekwatnie do wybranego zajęcia. 
        /// </summary>
        protected void setValues()
        {
            ListItem li = new ListItem();
            ListItem li2 = new ListItem();
            ListItem li3 = new ListItem();
            ListItem li4 = new ListItem();

            li.Text = values[0];
            li.Value = "1";
            PrzedmiotyDDL.Items.Add(li);

            DateTime temp = DateTime.ParseExact(values[1], "dd/MM/yyyy", CultureInfo.InvariantCulture);
            CalendarTextBox.Text = temp.ToString("yyyy-MM-dd");

            RozpoczecieTextBox.Text = values[2];
            ZakonczenieTextBox.Text = values[3];
            OpisTextBox.Text = values[4];

            if (bool.Parse(values[6]))
            {
                OgolnouczelnianeCheckBox.Checked = true;
                Wydzial.Visible = false;
                Kierunek.Visible = false;
            }
            else
            {
                OgolnouczelnianeCheckBox.Checked = false;
                Wydzial.Visible = true;
                Kierunek.Visible = true;

                li2.Text = values[7];
                li2.Value = "1";
                WydzialDDL.Items.Add(li2);

                li3.Text = values[8];
                li3.Value = "1";
                KierunekDDL.Items.Add(li3);
            }

            if (bool.Parse(values[9]))
            {
                ZdalneCheckBox.Checked = true;
                Sala.Visible = false;
            }
            else
            {
                ZdalneCheckBox.Checked = false;
                Sala.Visible = true;
                SalaTextBox.Text = values[10];
            }

            if (string.IsNullOrEmpty(values[12]) || values[12] == " ")
            {
                ZastepstwoCheckBox.Checked = false;
                Zastepstwo.Visible = false;
            }
            else
            {
                ZastepstwoCheckBox.Checked = true;
                Zastepstwo.Visible = true;
                li4.Text = values[12];
                li4.Value = "1";
                ZastepstwoDDL.Items.Add(li4);
            }

            TematTextBox.Text = values[13];
        }

        
        /// <summary>
        /// Pobieranie Id wydziału według oznaczenia.
        /// @param ozn Oznaczenie wydziału.
        /// @returns result Id wydziału.
        /// </summary>
        protected int getIdWydzial(string ozn)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {

                // Łączenie z bazą danych
                con.Open();

                // Zapytanie SQL
                string query = @"select Id from dbo.Wydzialy where Oznaczenie='" + ozn + "'";

                SqlCommand sql = new SqlCommand(query, con);

                int result = Convert.ToInt32(sql.ExecuteScalar());
                con.Close();

                return result;
            }
        }


        /// <summary>
        /// Pobieranie Id kierunku według oznaczenia.
        /// @param ozn Oznaczenie kierunku.
        /// @returns result Id kierunku.
        /// </summary>
        public int getIdKierunki(string ozn, string connectionString)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {

                // Łączenie z bazą danych
                con.Open();

                // Zapytanie SQL
                string query = @"select Id from dbo.Kierunki where Oznaczenie='" + ozn + "'";

                SqlCommand sql = new SqlCommand(query, con);

                int result = Convert.ToInt32(sql.ExecuteScalar());
                con.Close();

                return result;
            }
        }


        /// <summary>
        /// Sprawdzenie czy checkbox Ogólnouczelniane jest zaznaczony, jeśli tak to pola wydział i kierunek nie są dostępne.
        /// </summary>
        protected void OgolnouczelnianeCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (OgolnouczelnianeCheckBox.Checked)
            {
                Wydzial.Visible = false;
                Kierunek.Visible = false;
            }
            else
            {
                Wydzial.Visible = true;
                Kierunek.Visible = true;
            }
        }


        /// <summary>
        /// Sprawdzenie czy checkbox Zdalne jest zaznaczony, jeśli tak to pole Sala jest niedostępne.
        /// </summary>
        protected void ZdalneCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ZdalneCheckBox.Checked)
                Sala.Visible = false;
            else
                Sala.Visible = true;
        }


        /// <summary>
        /// Sprawdzenie czy checkbox Zastępstwo jest zaznaczony, jeśli tak to pole do wprowadzenia nauczyciela do zastępstwa staje się dostępne.
        /// </summary>
        protected void ZastepstwoCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ZastepstwoCheckBox.Checked)
                Zastepstwo.Visible = true;
            else
                Zastepstwo.Visible = false;
        }


        /// <summary>
        /// Pobieranie Id aktualnie zalogowanego pracownika.
        /// @param query Zapytanie SQL pobierające Id pracownika.
        /// @returns result Id pracownika.
        /// </summary>
        protected int returnPracownikId()
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();

                // Zapytanie SQL
                string query = @"select Id from dbo.Pracownicy
                            where IdProfilUzytkownika = '" + Session["unique"] + "'";

                SqlCommand sql = new SqlCommand(query, con);

                int result = Convert.ToInt32(sql.ExecuteScalar());
                con.Close();

                return result;
            }
        }
    }
}