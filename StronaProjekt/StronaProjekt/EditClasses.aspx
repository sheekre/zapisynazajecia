﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditClasses.aspx.cs" Inherits="StronaProjekt.EditClasses" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4" style="padding-left: 100px; padding-right: 100px;">
            <h2>Edytuj zajęcia.</h2>
            <p>
                Przedmiot:
                <asp:DropDownList ID="PrzedmiotyDDL" runat="server"></asp:DropDownList>
            </p>
            <p>
                Data:
                <asp:TextBox ID="CalendarTextBox" runat="server" TextMode="Date"></asp:TextBox>
            </p>
            <p>
                Godzina rozpoczecia:
                <asp:TextBox ID="RozpoczecieTextBox" runat="server" TextMode="Time"></asp:TextBox>
            </p>
            <p>
                Godzina zakończenia:
                <asp:TextBox ID="ZakonczenieTextBox" runat="server" TextMode="Time"></asp:TextBox>
            </p>
            <p>
                Opis:
                <asp:TextBox ID="OpisTextBox" runat="server" TextMode="MultiLine" Rows="5" style="resize:none;"></asp:TextBox>
            </p>
            <p>
                Ogolnouczelniane:
                <asp:CheckBox ID="OgolnouczelnianeCheckBox" runat="server" OnCheckedChanged="OgolnouczelnianeCheckBox_CheckedChanged" AutoPostBack="true" />
            </p>
            <p runat="server" visible="true" id="Wydzial">
                Wydział:
                <asp:DropDownList ID="WydzialDDL" runat="server"></asp:DropDownList>
            </p>
            <p runat="server" visible="true" id="Kierunek">
                Kierunek:
                <asp:DropDownList ID="KierunekDDL" runat="server"></asp:DropDownList>
            </p>
            <p>
                Zdalnie:
                <asp:CheckBox ID="ZdalneCheckBox" runat="server" OnCheckedChanged="ZdalneCheckBox_CheckedChanged" AutoPostBack="true"/>
            </p>
            <p runat="server" visible="true" id="Sala">
                Sala:
                <asp:TextBox ID="SalaTextBox" runat="server"></asp:TextBox>
            </p>
            <p>
                Zastępstwo:
                <asp:CheckBox ID="ZastepstwoCheckBox" runat="server" OnCheckedChanged="ZastepstwoCheckBox_CheckedChanged" AutoPostBack="true"/>
            </p>
            <p runat="server" visible="false" id="Zastepstwo">
                Nauczyciel zastępujący:
                <asp:DropDownList ID="ZastepstwoDDL" runat="server"></asp:DropDownList>
            </p>
            <p>
                Temat:
                <asp:TextBox ID="TematTextBox" runat="server" TextMode="MultiLine" Rows="5" MaxLength="100" style="resize:none;"></asp:TextBox>
            </p>
            <p>
                <asp:Button ID="EditButton" runat="server" Text="Zapisz zmiany" OnClick="EditButton_Click" />
                <asp:Button ID="CancellButton" runat="server" Text="Odwołaj zajecia" OnClick="CancellButton_Click" Visible="false"/>
                <asp:Button ID="RestoreButton" runat="server" Text="Przywróć zajecia" OnClick="RestoreButton_Click" Visible="false"/>
            </p>
            <p>
            <asp:Label ID="lblSuccessMessage" Text="" runat="server" ForeColor="Green"/>
            <asp:Label ID="lblErrorMessage" Text="" runat="server" ForeColor="Red" />
            </p>

        </div>
        <div class="col-md-4">
        </div>
    </div>

    <div runat="server" visible="false">

    </div>



</asp:Content>
