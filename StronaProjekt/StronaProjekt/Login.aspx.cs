﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StronaProjekt
{
    /// <summary>
    /// Logowanie.
    /// 
    /// Klasa obsugująca system logowania.
    /// @param connectionString Zmienna do przechowywania Connection Stringa do bazy danych.
    /// </summary>
    public partial class Login : System.Web.UI.Page
    {
        string connectionString = "";


        /// <summary>
        /// Wczytanie strony i bazy danych podczas ładowania strony.
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {   
            // Wczytanie connectionStringa do bazy danych zapisanego w Web.config do zmiennej (Podczas ładowania strony)
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnection"].ConnectionString;     
        }


        /// <summary>
        /// Metoda przeprowadzająca proces logowania.
        /// 
        /// Metoda porównuje wprowadzone dane z danymi z bazy danych, jeżeli dane są poprawne następuje zalogowanie, a jeżeli nie to wyskakuje komunikat błędu.
        /// @param query Zmienna przechowująca zapytanie SQL.
        /// @param count Zmienna przechowująca wynik zapytania.
        /// @param rdr To SqlDataReader.
        /// @param permission Przechowuje Id roli przypisanej do konta na które się logujemy.
        /// @param active Przechowuje wartość aktywności konta.
        /// @param unique przechowuje wartość uniqueidentifier czyli w tym przypadku Id konta.
        /// </summary>
        protected void LoginButton_Click(object sender, EventArgs e)
        {
            // Utworzenie połączenia z bazą danych w celu sprawdzenia poprawności logowania
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                // Łączenie z bazą danych
                con.Open();

                // Zapytanie SQL
                string query = "SELECT COUNT(1) FROM dbo.ProfileUzytkownikow WHERE Login=@login AND Password=@password";

                // Wstawienie agrumentów z TextBoxów do zapytania
                SqlCommand sql = new SqlCommand(query, con);
                sql.Parameters.AddWithValue("@login", LoginTextBox.Text.Trim());
                sql.Parameters.AddWithValue("@password", PasswordTextBox.Text.Trim());

                // Zapisanie wyniku zapytania do zmiennej
                int count = Convert.ToInt32(sql.ExecuteScalar());

                if (count == 1)
                {
                    // Zapytanie wyciągające poziom permisji użytkowika
                    query = "SELECT IdRola,Active,Id FROM dbo.ProfileUzytkownikow WHERE Login=@login";

                    sql = new SqlCommand(query, con);
                    sql.Parameters.AddWithValue("@login", LoginTextBox.Text.Trim());

                    // Stworzenie DataReadera
                    SqlDataReader rdr = sql.ExecuteReader();
                    int permission = 0;
                    bool active = false;
                    Guid unique = Guid.Empty;

                    while (rdr.Read())
                    {
                        permission = Convert.ToInt32(rdr["IdRola"]); 
                        active = (bool)rdr["Active"];
                        unique = (Guid)rdr["Id"];
                    }

                    // Zamknięcie połączenia z bazą i readera
                    con.Close();
                    rdr.Close();

                    // Sprawdzenie czy konto jest aktywne
                    if (active)
                    {
                        // Stworzenie sesji dla użytkownika
                        Session["login"] = LoginTextBox.Text.Trim();
                        Session["permission"] = permission;
                        Session["unique"] = unique;

                        // Przeniesienie użytkownika na kolejną strone
                        Response.Redirect("Classes.aspx");
                    }
                    else
                    {
                        // Wyświetlenie komunikatu o nieudanej próbie zalogowania
                        ErrorMessageLabel.Visible = true;
                        ErrorMessageLabel.Text = "Konto nieaktywne! Skontaktuj się z administratorem.";

                    }
                    
                }
                else
                {
                    // Wyświetlenie komunikatu o nieudanej próbie zalogowania
                    ErrorMessageLabel.Visible = true;
                    ErrorMessageLabel.Text = "Błąd logowania! Sprawdź login i hasło.";

                    // Zamknięcie połączenia z bazą
                    con.Close();
                }
            }
        }
    }
}