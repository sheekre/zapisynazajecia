﻿<%@ Page Title="Strona Logowania" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="StronaProjekt.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>

    <h3>Login:</h3>
    <asp:TextBox ID="LoginTextBox" runat="server"></asp:TextBox>

    <h3>Hasło:</h3>
    <asp:TextBox ID="PasswordTextBox" runat="server"></asp:TextBox>

    <p></p>
        <asp:Button ID="LoginButton" runat="server" Text="Zaloguj" CssClass="btn btn-primary btn-lg" OnClick="LoginButton_Click" />
    <p></p>

    <asp:Label ID="ErrorMessageLabel" runat="server" Visible="False" ForeColor="Red"></asp:Label>
   
    
</asp:Content>
