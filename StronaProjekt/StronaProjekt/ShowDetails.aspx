﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ShowDetails.aspx.cs" Inherits="StronaProjekt.ShowDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

     <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4" style="padding-left: 100px; padding-right: 100px;">
            <h2>Szczegóły zajęć.</h2>
            <p>
                Przedmiot:
                <asp:DropDownList ID="PrzedmiotyDDL" runat="server" Enabled="False"></asp:DropDownList>
            </p>
            <p>
                Data:
                <asp:TextBox ID="CalendarTextBox" runat="server" TextMode="Date" Enabled="False"></asp:TextBox>
            </p>
            <p>
                Godzina rozpoczecia:
                <asp:TextBox ID="RozpoczecieTextBox" runat="server" TextMode="Time" Enabled="False"></asp:TextBox>
            </p>
            <p>
                Godzina zakończenia:
                <asp:TextBox ID="ZakonczenieTextBox" runat="server" TextMode="Time" Enabled="False"></asp:TextBox>
            </p>
            <p>
                Opis:
                <asp:TextBox ID="OpisTextBox" runat="server" TextMode="MultiLine" Rows="5" style="resize:none;" Enabled="False"></asp:TextBox>
            </p>
            <p>
                Ogolnouczelniane:
                <asp:CheckBox ID="OgolnouczelnianeCheckBox" runat="server" OnCheckedChanged="OgolnouczelnianeCheckBox_CheckedChanged" AutoPostBack="true" Enabled="False"/>
            </p>
            <p runat="server" visible="true" id="Wydzial">
                Wydział:
                <asp:DropDownList ID="WydzialDDL" runat="server" Enabled="False"></asp:DropDownList>
            </p>
            <p runat="server" visible="true" id="Kierunek">
                Kierunek:
                <asp:DropDownList ID="KierunekDDL" runat="server" Enabled="False"></asp:DropDownList>
            </p>
            <p>
                Zdalnie:
                <asp:CheckBox ID="ZdalneCheckBox" runat="server" OnCheckedChanged="ZdalneCheckBox_CheckedChanged" AutoPostBack="true" Enabled="False"/>
            </p>
            <p runat="server" visible="true" id="Sala">
                Sala:
                <asp:TextBox ID="SalaTextBox" runat="server" Enabled="False"></asp:TextBox>
            </p>
            <p>
                Zastępstwo:
                <asp:CheckBox ID="ZastepstwoCheckBox" runat="server" OnCheckedChanged="ZastepstwoCheckBox_CheckedChanged" AutoPostBack="true" Enabled="False"/>
            </p>
            <p runat="server" visible="false" id="Zastepstwo">
                Nauczyciel zastępujący:
                <asp:DropDownList ID="ZastepstwoDDL" runat="server" Enabled="False"></asp:DropDownList>
            </p>
            <p>
                Temat:
                <asp:TextBox ID="TematTextBox" runat="server" TextMode="MultiLine" Rows="5" MaxLength="100" style="resize:none;" Enabled="False"></asp:TextBox>
            </p>
            

        </div>
        <div class="col-md-4">
        </div>
    </div>


</asp:Content>
