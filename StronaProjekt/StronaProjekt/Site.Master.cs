﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StronaProjekt
{
    /// <summary>
    /// Klasa strony głównej.
    /// </summary>
    public partial class SiteMaster : MasterPage
    {
        /// <summary>
        /// Załadowanie strony głównej.
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {    
            if (Session["login"] != null)
            {
                // Ukryj przycisk do logowania i wyświetl do wylogowywania oraz przenoszacy na podstrone Classes
                LoginLinkButton.Visible = false;
                LogoutLinkButton.Visible = true;
                ClassesLinkButton.Visible = true;
                
                // Jeśli sesja istnieje wyświetl nazwe użytkownika
                UsernameLabel.Text = Session["login"].ToString();
                UsernameLabel.Visible = true;
            }
        }

        /// <summary>
        /// Obsługa przycisku zaloguj - pasek nawigacji.
        /// </summary>
        protected void LoginLinkButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("Login.aspx");
        }


        /// <summary>
        /// Obsługa przycisku wyloguj - pasek nawigacji.
        /// </summary>
        protected void LogoutLinkButton_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("Default.aspx");
        }

        /// <summary>
        /// Obsługa przycisku zajęcia - pasek nawigacji.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ClassesLinkButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("Classes.aspx");
        }


    }
}