﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StronaProjekt
{
    /// <summary>
    /// Widok zajęć.
    /// 
    /// Klasa wyświetlajace odpowiednie grid'y zajęć.
    /// @param connectionString Zmienna do przechowywania Connection Stringa do bazy danych.
    /// </summary>
    public partial class Classes : System.Web.UI.Page
    {
        // Zmienna do przechowywania connectionString'a
        string connectionString = "";


        /// <summary>
        /// Załadowanie strony i wczytanie Connection Stringa. W przypadku gdy ktoś wejdzie z linku bez sesji następuje przeniesienia ns stronę logowania. 
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Jeśli ktoś wejdzie z linku bez sesji przenieś na strone logowania
            if (Session["login"] == null)
                Response.Redirect("Login.aspx");

            // Wczytanie connectionStringa
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnection"].ConnectionString;

            // Wyświetl dane użytkownika
            //LoginLabel.Text = "Witaj " + Session["permission"] + "\t" + Session["unique"];

            if (!IsPostBack)
                LoadDataIntoGridViews();

            if (Session["permission"].ToString() != "3")
                AddClassesButton.Visible = true;

        }


        /// <summary>
        /// Załadowanie listy wszystkich zajęć w postaci GRID - lista dla kierownika.
        /// @param select Zapytanie SQL pobierające dane z bazy.
        /// </summary>
        protected void LoadClassesGridView()
        {
            var select = @"SELECT z.[Id]
                          ,prz.Nazwa AS 'Przedmiot'
                          ,FORMAT(z.[Data], 'dd/MM/yyyy') as [Data]
                          ,CONVERT(varchar(5), z.[GodzinaRozpoczecia], 108) AS 'Godzina rozpoczęcia'
                          ,CONVERT(varchar(5), z.[GodzinaZakonczenia], 108) AS 'Godzina zakończenia'
                          ,z.[Opis]
                          ,CONCAT(p.Imie, ' ', p.Nazwisko) AS 'Prowadzący'
                          ,[CzyOgolnoUczelniane] AS 'Ogólnouczelniane'
                          ,w.Oznaczenie AS 'Wydział'
                          ,k.Oznaczenie AS 'Kierunek'
                          ,z.[CzyZdalnie] AS 'Zdalne'
                          ,z.[IdSala] AS 'Sala'
                          ,z.[CzyOdwolane] AS 'Odwołane'
                          ,CONCAT(p1.Imie, ' ', p1.Nazwisko) AS 'Zastępstwo'
                          ,z.[Temat]
                        FROM [ZapisyNaZajecia].[dbo].[Zajecia] z
                        LEFT JOIN [ZapisyNaZajecia].[dbo].[Pracownicy] p ON z.IdPracownikProwadzacy = p.Id
                        LEFT JOIN [ZapisyNaZajecia].[dbo].[Pracownicy] p1 ON z.IdPracownikZastepstwo = p1.Id
                        LEFT JOIN [ZapisyNaZajecia].[dbo].[Wydzialy] w ON z.IdWydzial = w.Id
                        LEFT JOIN [ZapisyNaZajecia].[dbo].[Kierunki] k ON z.IdKierunek = k.Id
                        LEFT JOIN [ZapisyNaZajecia].[dbo].[Przedmioty] prz ON z.IdPrzedmiot = prz.Id";
            var c = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(select, c);

            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds);
            var dt = ds.Tables[0];
       
            ClassesGridView.DataSource = dt;  
            ClassesGridView.DataBind();
        }


        /// <summary>
        /// Załadowanie listy zajęć na które student jest zpaisany w postaci GRID - lista dla studenta.
        /// @param select Zapytanie SQL pobierające dane z bazy.
        /// </summary>
        protected void LoadMyClassesGridView()
        {
            var select = @"SELECT z.[Id]
                          ,prz.Nazwa AS 'Przedmiot'
                          ,FORMAT(z.[Data], 'dd/MM/yyyy') as [Data]
                          ,CONVERT(varchar(5), z.[GodzinaRozpoczecia], 108) AS 'Godzina rozpoczęcia'
                          ,CONVERT(varchar(5), z.[GodzinaZakonczenia], 108) AS 'Godzina zakończenia'
                          ,z.[Opis]
                          ,CONCAT(p.Imie, ' ', p.Nazwisko) AS 'Prowadzący'
                          ,[CzyOgolnoUczelniane] AS 'Ogólnouczelniane'
                          ,w.Oznaczenie AS 'Wydział'
                          ,k.Oznaczenie AS 'Kierunek'
                          ,z.[CzyZdalnie] AS 'Zdalne'
                          ,z.[IdSala] AS 'Sala'
                          ,z.[CzyOdwolane] AS 'Odwołane'
                          ,CONCAT(p1.Imie, ' ', p1.Nazwisko) AS 'Zastępstwo'
                          ,z.[Temat]
                      FROM [ZapisyNaZajecia].[dbo].[Zajecia] z
                      LEFT JOIN [ZapisyNaZajecia].[dbo].[Pracownicy] p ON z.IdPracownikProwadzacy = p.Id
                      LEFT JOIN [ZapisyNaZajecia].[dbo].[Pracownicy] p1 ON z.IdPracownikZastepstwo = p1.Id
                      LEFT JOIN [ZapisyNaZajecia].[dbo].[Wydzialy] w ON z.IdWydzial = w.Id
                      LEFT JOIN [ZapisyNaZajecia].[dbo].[Kierunki] k ON z.IdKierunek = k.Id
                      LEFT JOIN [ZapisyNaZajecia].[dbo].[Przedmioty] prz ON z.IdPrzedmiot = prz.Id
                      LEFT JOIN [ZapisyNaZajecia].[dbo].[ZajeciaStudenciLinki] zsl ON z.Id = zsl.IdZajecie
                      LEFT JOIN [ZapisyNaZajecia].[dbo].[Studenci] s ON zsl.IdStudent = s.Id
                      LEFT JOIN [ZapisyNaZajecia].[dbo].[ProfileUzytkownikow] pu ON s.IdProfilUzytkownika = pu.Id
                      WHERE pu.Id = '" + Session["unique"] + "'";
            var c = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(select, c);

            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds);
            var dt = ds.Tables[0];

            MyClassesGridView.DataSource = dt;
            MyClassesGridView.DataBind();
        }


        /// <summary>
        /// Załadowanie listy prowadzonych przez zalogowanego nauczyciela zajęć w postaci GRID - lista dla nauczyciela.
        /// @param select Zapytanie SQL pobierające dane z bazy.
        /// </summary>
        protected void LoadTeacherGridView()
        {
            var select = @"SELECT z.[Id]
                          ,prz.Nazwa AS 'Przedmiot'
                          ,FORMAT(z.[Data], 'dd/MM/yyyy') as [Data]
                          ,CONVERT(varchar(5), z.[GodzinaRozpoczecia], 108) AS 'Godzina rozpoczęcia'
                          ,CONVERT(varchar(5), z.[GodzinaZakonczenia], 108) AS 'Godzina zakończenia'
                          ,z.[Opis]
                          ,CONCAT(p.Imie, ' ', p.Nazwisko) AS 'Prowadzący'
                          ,[CzyOgolnoUczelniane] AS 'Ogólnouczelniane'
                          ,w.Oznaczenie AS 'Wydział'
                          ,k.Oznaczenie AS 'Kierunek'
                          ,z.[CzyZdalnie] AS 'Zdalne'
                          ,z.[IdSala] AS 'Sala'
                          ,z.[CzyOdwolane] AS 'Odwołane'
                          ,CONCAT(p1.Imie, ' ', p1.Nazwisko) AS 'Zastępstwo'
                          ,z.[Temat]
                      FROM [ZapisyNaZajecia].[dbo].[Zajecia] z
                      LEFT JOIN [ZapisyNaZajecia].[dbo].[Pracownicy] p ON z.IdPracownikProwadzacy = p.Id
                      LEFT JOIN [ZapisyNaZajecia].[dbo].[Pracownicy] p1 ON z.IdPracownikZastepstwo = p1.Id
                      LEFT JOIN [ZapisyNaZajecia].[dbo].[Wydzialy] w ON z.IdWydzial = w.Id
                      LEFT JOIN [ZapisyNaZajecia].[dbo].[Kierunki] k ON z.IdKierunek = k.Id
                      LEFT JOIN [ZapisyNaZajecia].[dbo].[Przedmioty] prz ON z.IdPrzedmiot = prz.Id
                       WHERE p.IdProfilUzytkownika = '" + Session["unique"] + "'";
            var c = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(select, c);

            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds);
            var dt = ds.Tables[0];

            TeacherGridView.DataSource = dt;
            TeacherGridView.DataBind();
        }


        /// <summary>
        /// Załadowanie listy dostępnych do zapisania się zajęć w postaci GRID - lista dla studenta.
        /// @param select Zapytanie SQL pobierające dane z bazy.
        /// </summary>
        protected void LoadEnrollGridView()
        {
            var select = @"SELECT z.[Id]
                          ,prz.Nazwa AS 'Przedmiot'
                          ,FORMAT(z.[Data], 'dd/MM/yyyy') as [Data]
                          ,CONVERT(varchar(5), z.[GodzinaRozpoczecia], 108) AS 'Godzina rozpoczęcia'
                          ,CONVERT(varchar(5), z.[GodzinaZakonczenia], 108) AS 'Godzina zakończenia'
                          ,z.[Opis]
                          ,CONCAT(p.Imie, ' ', p.Nazwisko) AS 'Prowadzący'
                          ,[CzyOgolnoUczelniane] AS 'Ogólnouczelniane'
                          ,w.Oznaczenie AS 'Wydział'
                          ,k.Oznaczenie AS 'Kierunek'
                          ,z.[CzyZdalnie] AS 'Zdalne'
                          ,z.[IdSala] AS 'Sala'
                          ,z.[CzyOdwolane] AS 'Odwołane'
                          ,CONCAT(p1.Imie, ' ', p1.Nazwisko) AS 'Zastępstwo'
                          ,z.[Temat]
                      FROM[ZapisyNaZajecia].[dbo].[Zajecia] z
                     LEFT JOIN[ZapisyNaZajecia].[dbo].[Pracownicy] p ON z.IdPracownikProwadzacy = p.Id
                      LEFT JOIN[ZapisyNaZajecia].[dbo].[Pracownicy] p1 ON z.IdPracownikZastepstwo = p1.Id
                      LEFT JOIN[ZapisyNaZajecia].[dbo].[Wydzialy] w ON z.IdWydzial = w.Id
                      LEFT JOIN[ZapisyNaZajecia].[dbo].[Kierunki] k ON z.IdKierunek = k.Id
                      LEFT JOIN[ZapisyNaZajecia].[dbo].[Przedmioty] prz ON z.IdPrzedmiot = prz.Id
                      LEFT JOIN[ZapisyNaZajecia].[dbo].[ZajeciaStudenciLinki] zsl ON z.Id = zsl.IdZajecie
                      LEFT JOIN[ZapisyNaZajecia].[dbo].[Studenci] s ON zsl.IdStudent = s.Id
                      LEFT JOIN[ZapisyNaZajecia].[dbo].[ProfileUzytkownikow] pu ON s.IdProfilUzytkownika = pu.Id
                      WHERE pu.Id <> '" + Session["unique"] + "' OR pu.Id IS NULL";

            var c = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(select, c);

            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds);
            var dt = ds.Tables[0];
            
            EnrollGridView.DataSource = dt;
            EnrollGridView.DataBind();
        }

        /// <summary>
        /// Obsługa zapisu na zajęcie.
        /// @param query Zapytanie INSERT zapisujące dane do bazy. Zawiera Id zapisywanego studenta oraz zajęcia na które chce dokonać zapisu.
        /// </summary>
        protected void EnrollGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //Obsługa przycisku zapisz
            if (e.CommandName == "Enroll")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = EnrollGridView.Rows[index];


                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    // Łączenie z bazą danych
                    con.Open();

                    // Zapytanie SQL
                    string query = @"INSERT INTO dbo.ZajeciaStudenciLinki(IdStudent,IdZajecie)
                                    VALUES (" + GetStudentId() + "," + row.Cells[1].Text + ");";

                    SqlCommand sql = new SqlCommand(query, con);
                    sql.ExecuteNonQuery();

                    con.Close();
                    LoadDataIntoGridViews();
                }
            }            
        }


        /// <summary>
        /// Funkcja łącząca imię i nazwisko pobrane z bazy.
        /// @param name Imię
        /// @param surname Nazwisko
        /// @returns Imię Nazwisko
        /// </summary>
        public string ConcatNameAndSurname(string name, string surname)
        {
            return name + " " + surname;
        }


        /// <summary>
        /// Obsługa przycisku Szczegóły.
        /// </summary>
        protected void MyClassesGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //Obsługa przycisku pokaż szczegóły
            if (e.CommandName == "ShowDetails")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = MyClassesGridView.Rows[index];
                CheckBox ogolno = row.Cells[8].Controls[0] as CheckBox;
                CheckBox zdalne = row.Cells[11].Controls[0] as CheckBox;
                CheckBox odwolane = row.Cells[13].Controls[0] as CheckBox;

                List<string> details = new List<string>() { row.Cells[2].Text, row.Cells[3].Text, row.Cells[4].Text, row.Cells[5].Text, row.Cells[6].Text,
                row.Cells[7].Text, ogolno.Checked.ToString(), row.Cells[9].Text, row.Cells[10].Text,zdalne.Checked.ToString(),row.Cells[12].Text,odwolane.Checked.ToString(),row.Cells[14].Text,row.Cells[15].Text};
                Session["details"] = details;
                Session["resultId"] = row.Cells[1].Text;
                Response.Redirect("ShowDetails.aspx");
            }
        }


        /// <summary>
        /// Obsługa przycisku edytuj.
        /// </summary>
        protected void TeacherGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //Obsługa przycisku edytuj
            if (e.CommandName == "EditDetails")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = TeacherGridView.Rows[index];
                CheckBox ogolno = row.Cells[8].Controls[0] as CheckBox;
                CheckBox zdalne = row.Cells[11].Controls[0] as CheckBox;
                CheckBox odwolane = row.Cells[13].Controls[0] as CheckBox;
                
                List<string> details = new List<string>() { row.Cells[2].Text, row.Cells[3].Text, row.Cells[4].Text, row.Cells[5].Text, row.Cells[6].Text,
                row.Cells[7].Text, ogolno.Checked.ToString(), row.Cells[9].Text, row.Cells[10].Text,zdalne.Checked.ToString(),row.Cells[12].Text,odwolane.Checked.ToString(),row.Cells[14].Text,row.Cells[15].Text};
                Session["details"] = details;
                Session["resultId"] = row.Cells[1].Text;
                Response.Redirect("EditClasses.aspx");
            }
        }


        /// <summary>
        /// Pobieranie Id studenta.
        /// @param query Zapytanie pobierające Id studenta.
        /// @returns result Id studenta
        /// </summary>
        protected int GetStudentId()
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {

                // Łączenie z bazą danych
                con.Open();

                // Zapytanie SQL
                string query = @"SELECT TOP 1 s.Id
                                FROM dbo.Studenci s
                                LEFT JOIN dbo.ProfileUzytkownikow pu ON s.IdProfilUzytkownika = pu.Id
                                WHERE pu.Id = '63C9CA45-3A1A-4FC5-B366-92ED9298D950'";


                SqlCommand sql = new SqlCommand(query, con);


                int result = Convert.ToInt32(sql.ExecuteScalar());
                con.Close();

                return result;
            }
        }


        /// <summary>
        /// Wczytanie danych do GRIDów.
        /// </summary>
        protected void LoadDataIntoGridViews()
        {
            // Wyczytaj zawartość tabeli Zajecia do ClassesGridView (Tylko dla Admina)
            if (Session["permission"].ToString() == "1")
            {
                ClassesDiv.Visible = true;
                LoadClassesGridView();
            }

            // Wyczytaj zapisy na zajęcia i zajecia na ktore uczeszcza (Tylko dla Studenta)
            else if (Session["permission"].ToString() == "3")
            {
                EnrollDiv.Visible = true;
                MyClassesDiv.Visible = true;
                LoadEnrollGridView();
                LoadMyClassesGridView();

            }
            // Tylko dla nauczyciela
            else if (Session["permission"].ToString() == "2")
            {
                TeacherDiv.Visible = true;
                LoadTeacherGridView();
            }
        }

   
        /// <summary>
        /// Obsługa przycisku dodawania zajęć.
        /// </summary>
        protected void AddClassesButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddClasses.aspx");
        }
    }
}

