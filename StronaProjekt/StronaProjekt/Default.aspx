﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="StronaProjekt._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Inżynieria oprogramowania - projekt zaliczeniowy.</h1>
        <p class="lead">Program obsługujący zapisy na zajęcia na wyższych uczelniach z podziałem dostępnych funkcjonalności w zależności od permmission zalogowanego użytkownika.</p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Pracownik/Kierownik</h2>
            <p>
                Administruje aplikacją i nadzoruje ruch. Ma wgląd w to jakie zajęcia są prowadzone na uczelni, aktywność prowadzących i aktywność uczniów.
            </p>
        </div>
        <div class="col-md-4">
            <h2>Prowadzący/Nauczyciel</h2>
            <p>
                Dodaje prowadzone przez siebie zajęcia do harmonogramu podając: typ (zdalne, stacjonarne), wydział, kierunek oraz rok, datę wraz z godziną odbywania się zajęć, planowaną datę zakończenia zajęć, przedmiot. Pracownik może odwołać zajęcia lub przenieść je na inny termin.
            </p>
        </div>
        <div class="col-md-4">
            <h2>Student</h2>
            <p>
                Po zalogowaniu się do serwisu zostaje wyświetlona strona główna np. na której będzie rozpiska/harmonogram zajęć na które się zapisał. Następnie przechodząc do zakładki zapisu na zajęcia program wyświetli mu listę zajęć (domyślnie wszystkich dla jego kierunku). Student będzie mieć do dyspozycji odpowiednie filtry aby wyszukać interesujące go zajęcia prowadzone na uczelni
            </p>
        </div>
    </div>

</asp:Content>
