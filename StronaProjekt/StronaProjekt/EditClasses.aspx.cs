﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StronaProjekt
{
    /// <summary>
    /// Edytowanie zajęć.
    /// 
    /// Klasa odpowiadająca za edycję zajęć przez prowadzącego.
    /// @param connectionString Zmienna do przechowywania Connection Stringa do bazy danych.
    /// @param values Zmienna przechowująca wartości pobrane z bazy.
    /// </summary>
    public partial class EditClasses : System.Web.UI.Page
    {
        // Zmienna do przechowywania connectionString'a
        string connectionString = "";
        List<string> values = new List<string>();


        /// <summary>
        /// Załadowanie strony i wczytanie Connection Stringa. W przypadku gdy ktoś wejdzie z linku bez sesji następuje przeniesienia ns stronę logowania. Jeśli użytkownik jest zalogowany następuje załadowanie danych.
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["login"] == null)
                Response.Redirect("Login.aspx");
            if (Session["permission"].ToString() == "3")
                Response.Redirect("Default.aspx");

            // Wczytanie connectionStringa
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DatabaseConnection"].ConnectionString;
            values = (List<string>)Session["details"];

            if (!IsPostBack)
            {
                loadData();
                setValues();

                if (bool.Parse(values[11]))
                    RestoreButton.Visible = true;
                else
                    CancellButton.Visible = true;
            }
        }
        
        /// <summary>
        /// Inicjacja załadowania danych.
        /// </summary>
        protected void loadData()
        {
            loadPrzedmioty();
            loadWydzial();
            loadKierunek();
            loadZastepstwo();
        }


        /// <summary>
        /// Ustawienie wartości według pobranych danych z bazy.
        /// </summary>
        protected void setValues()
        {
            PrzedmiotyDDL.Items.FindByText(values[0]).Selected = true;

            DateTime temp = DateTime.ParseExact(values[1], "dd/MM/yyyy", CultureInfo.InvariantCulture);
            CalendarTextBox.Text = temp.ToString("yyyy-MM-dd");

            RozpoczecieTextBox.Text = values[2];
            ZakonczenieTextBox.Text = values[3];
            OpisTextBox.Text = values[4];

            if (bool.Parse(values[6]))
            {
                OgolnouczelnianeCheckBox.Checked = true;
                Wydzial.Visible = false;
                Kierunek.Visible = false;
            }
            else
            {
                OgolnouczelnianeCheckBox.Checked = false;
                Wydzial.Visible = true;
                Kierunek.Visible = true;

                WydzialDDL.Items.FindByValue(getIdWydzial(values[7]).ToString()).Selected = true;
                KierunekDDL.Items.FindByValue(getIdKierunki(values[8]).ToString()).Selected = true;
            }

            if (bool.Parse(values[9]))
            {
                ZdalneCheckBox.Checked = true;
                Sala.Visible = false;
            }
            else
            {
                ZdalneCheckBox.Checked = false;
                Sala.Visible = true;
                SalaTextBox.Text = values[10];
            }

            if (string.IsNullOrEmpty(values[12]) || values[12] == " ")
            {
                ZastepstwoCheckBox.Checked = false;
                Zastepstwo.Visible = false;
            }
            else
            { 
                ZastepstwoCheckBox.Checked = true;
                Zastepstwo.Visible = true;
                ZastepstwoDDL.Items.FindByText(values[12]).Selected = true;
            }

            TematTextBox.Text = values[13];
        }


        /// <summary>
        /// Załadowanie lsity przedmiotów.
        /// </summary>
        protected void loadPrzedmioty()
        {
            var select = @"select Id,Nazwa from dbo.Przedmioty";
            var c = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(select, c);

            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds);
            var dt = ds.Tables[0];

            PrzedmiotyDDL.DataTextField = dt.Columns["Nazwa"].ToString();
            PrzedmiotyDDL.DataValueField = dt.Columns["Id"].ToString();

            PrzedmiotyDDL.DataSource = dt;
            PrzedmiotyDDL.DataBind();
        }


        /// <summary>
        /// Załadowanie listy wydziałów.
        /// </summary>
        protected void loadWydzial()
        {
            var select = @"select Id, CONCAT(Nazwa, ' (', Oznaczenie,  ')') AS 'Wydzial' from dbo.Wydzialy";
            var c = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(select, c);

            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds);
            var dt = ds.Tables[0];

            WydzialDDL.DataTextField = dt.Columns["Wydzial"].ToString();
            WydzialDDL.DataValueField = dt.Columns["Id"].ToString();

            WydzialDDL.DataSource = dt;
            WydzialDDL.DataBind();
        }


        /// <summary>
        /// Załadowanie listy kierunków.
        /// </summary>
        protected void loadKierunek()
        {
            var select = @"select Id, CONCAT(Nazwa, ' (', Oznaczenie,  ')') AS 'Kierunek' from dbo.Kierunki";
            var c = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(select, c);

            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds);
            var dt = ds.Tables[0];

            KierunekDDL.DataTextField = dt.Columns["Kierunek"].ToString();
            KierunekDDL.DataValueField = dt.Columns["Id"].ToString();

            KierunekDDL.DataSource = dt;
            KierunekDDL.DataBind();
        }


        /// <summary>
        /// Załadowanie listy pracowników do zastępstwa.
        /// </summary>
        protected void loadZastepstwo()
        {
            var select = @"select Id, CONCAT(Imie, ' ', Nazwisko) AS 'Nazwa' from dbo.Pracownicy
                           where IdProfilUzytkownika <> '" + Session["unique"] + "'";
            var c = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(select, c);

            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds);
            var dt = ds.Tables[0];

            ZastepstwoDDL.DataTextField = dt.Columns["Nazwa"].ToString();
            ZastepstwoDDL.DataValueField = dt.Columns["Id"].ToString();

            ZastepstwoDDL.DataSource = dt;
            ZastepstwoDDL.DataBind();
        }


        /// <summary>
        /// Pobieranie Id wydziału według oznaczenia.
        /// @param ozn Oznaczenie wydziału.
        /// @returns result Id wydziału.
        /// </summary>
        protected int getIdWydzial(string ozn)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                // Łączenie z bazą danych
                con.Open();

                // Zapytanie SQL
                string query = @"select Id from dbo.Wydzialy where Oznaczenie='" + ozn + "'";

                SqlCommand sql = new SqlCommand(query, con);

                int result = Convert.ToInt32(sql.ExecuteScalar());
                con.Close();

                return result;
            }
        }


        /// <summary>
        /// Pobieranie Id kierunku według oznaczenia.
        /// @param ozn Oznaczenie kierunku.
        /// @returns result Id kierunku.
        /// </summary>
        protected int getIdKierunki(string ozn)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                // Łączenie z bazą danych
                con.Open();

                // Zapytanie SQL
                string query = @"select Id from dbo.Kierunki where Oznaczenie='" + ozn + "'";

                SqlCommand sql = new SqlCommand(query, con);

                int result = Convert.ToInt32(sql.ExecuteScalar());
                con.Close();

                return result;
            }
        }


        /// <summary>
        /// Sprawdzenie czy checkbox Ogólnouczelniane jest zaznaczony, jeśli tak to pola wydział i kierunek nie są dostępne.
        /// </summary>
        protected void OgolnouczelnianeCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (OgolnouczelnianeCheckBox.Checked)
            {
                Wydzial.Visible = false;
                Kierunek.Visible = false;
            }
            else
            {
                Wydzial.Visible = true;
                Kierunek.Visible = true;
            }
        }


        /// <summary>
        /// Sprawdzenie czy checkbox Zdalne jest zaznaczony, jeśli tak to pole Sala jest niedostępne.
        /// </summary>
        protected void ZdalneCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ZdalneCheckBox.Checked)
                Sala.Visible = false;
            else
                Sala.Visible = true;
        }


        /// <summary>
        /// Sprawdzenie czy checkbox Zastępstwo jest zaznaczony, jeśli tak to pole do wprowadzenia nauczyciela do zastępstwa staje się dostępne.
        /// </summary>
        protected void ZastepstwoCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ZastepstwoCheckBox.Checked)
                Zastepstwo.Visible = true;
            else
                Zastepstwo.Visible = false;
        }


        /// <summary>
        /// Pobieranie Id aktualnie zalogowanego pracownika.
        /// @param query Zapytanie SQL pobierające Id pracownika.
        /// @returns result Id pracownika.
        /// </summary>
        protected int returnPracownikId()
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();

                // Zapytanie SQL
                string query = @"select Id from dbo.Pracownicy
                            where IdProfilUzytkownika = '" + Session["unique"] + "'";


                SqlCommand sql = new SqlCommand(query, con);


                int result = Convert.ToInt32(sql.ExecuteScalar());
                con.Close();

                return result;
            }
        }


        /// <summary>
        /// Obsługa zapisu zedytowanych wartości.
        /// @param query Zapytanie SQL wykonujące UPDATE.
        /// </summary>
        protected void EditButton_Click(object sender, EventArgs e)
        {
            //Jesli wszystkie pola zostaly wypelnione
            if (CalendarTextBox.Text.Length > 0 && RozpoczecieTextBox.Text.Length > 0 && ZakonczenieTextBox.Text.Length > 0 &&
                OpisTextBox.Text.Length > 0 && TematTextBox.Text.Length > 0)
            {
                // Jesli nie zdalne
                if (ZdalneCheckBox.Checked == false)
                {
                    if (SalaTextBox.Text.Length > 0)
                    {
                        using (SqlConnection con = new SqlConnection(connectionString))
                        {
                            con.Open();

                            // Zapytanie SQL
                            string query = @"UPDATE [dbo].[Zajecia] SET
                                [IdPrzedmiot] = @IdPrzedmiot,[Data]= @Data,[GodzinaRozpoczecia] = @GodzinaRozpoczecia,[GodzinaZakonczenia] = @GodzinaZakonczenia,[Opis] = @Opis,
                                [IdPracownikProwadzacy] = @IdPracownikProwadzacy,[CzyOgolnoUczelniane] = @CzyOgolnoUczelniane,[IdWydzial] = @IdWydzial,[IdKierunek] = @IdKierunek,
                                [CzyZdalnie] = @CzyZdalnie,[IdSala] = @IdSala,[CzyOdwolane] = " + isCancelled() + @",[CzyZastpestwo] = @CzyZastpestwo,
                                [IdPracownikZastepstwo] = @IdPracownikZastepstwo, [Temat] =  @Temat
                                WHERE Id = " + Session["resultId"];
                                
                            SqlCommand sqlCmd = new SqlCommand(query, con);
                            sqlCmd.Parameters.AddWithValue("@IdPrzedmiot", PrzedmiotyDDL.SelectedItem.Value);
                            sqlCmd.Parameters.AddWithValue("@Data", CalendarTextBox.Text);
                            sqlCmd.Parameters.AddWithValue("@GodzinaRozpoczecia", RozpoczecieTextBox.Text + ":00");
                            sqlCmd.Parameters.AddWithValue("@GodzinaZakonczenia", ZakonczenieTextBox.Text + ":00");
                            sqlCmd.Parameters.AddWithValue("@Opis", OpisTextBox.Text);
                            sqlCmd.Parameters.AddWithValue("@IdPracownikProwadzacy", returnPracownikId());
                            sqlCmd.Parameters.AddWithValue("@CzyOgolnoUczelniane", OgolnouczelnianeCheckBox.Checked);

                            if (OgolnouczelnianeCheckBox.Checked == false)
                            {
                                sqlCmd.Parameters.AddWithValue("@IdWydzial", WydzialDDL.SelectedItem.Value);
                                sqlCmd.Parameters.AddWithValue("@IdKierunek", KierunekDDL.SelectedItem.Value);
                            }
                            else if (OgolnouczelnianeCheckBox.Checked)
                            {
                                sqlCmd.Parameters.AddWithValue("@IdWydzial", DBNull.Value);
                                sqlCmd.Parameters.AddWithValue("@IdKierunek", DBNull.Value);
                            }

                            sqlCmd.Parameters.AddWithValue("@CzyZdalnie", ZdalneCheckBox.Checked);
                            sqlCmd.Parameters.AddWithValue("@IdSala", SalaTextBox.Text);

                            sqlCmd.Parameters.AddWithValue("@CzyZastpestwo", ZastepstwoCheckBox.Checked);
                            if (ZastepstwoCheckBox.Checked == false)
                                sqlCmd.Parameters.AddWithValue("@IdPracownikZastepstwo", DBNull.Value);
                            else if (ZastepstwoCheckBox.Checked)
                                sqlCmd.Parameters.AddWithValue("@IdPracownikZastepstwo", ZastepstwoDDL.SelectedItem.Value);

                            sqlCmd.Parameters.AddWithValue("@Temat", TematTextBox.Text);

                            sqlCmd.ExecuteScalar();
                            lblSuccessMessage.Text = "Zaktualizowano rekord.";
                            lblErrorMessage.Text = "";
                            con.Close();
                        }
                    }
                    else
                    {
                        lblSuccessMessage.Text = "";
                        lblErrorMessage.Text = "Wpisz numer sali.";
                    }

                }
                else
                {
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();

                        // Zapytanie SQL
                        string query = @"UPDATE [dbo].[Zajecia] SET
                                [IdPrzedmiot] = @IdPrzedmiot,[Data]= @Data,[GodzinaRozpoczecia] = @GodzinaRozpoczecia,[GodzinaZakonczenia] = @GodzinaZakonczenia,[Opis] = @Opis,
                                [IdPracownikProwadzacy] = @IdPracownikProwadzacy,[CzyOgolnoUczelniane] = @CzyOgolnoUczelniane,[IdWydzial] = @IdWydzial,[IdKierunek] = @IdKierunek,
                                [CzyZdalnie] = @CzyZdalnie,[IdSala] = @IdSala,[CzyOdwolane] = " + isCancelled() + @",[CzyZastpestwo] = @CzyZastpestwo,
                                [IdPracownikZastepstwo] = @IdPracownikZastepstwo, [Temat] =  @Temat
                                WHERE Id = " + Session["resultId"];

                        SqlCommand sqlCmd = new SqlCommand(query, con);
                        sqlCmd.Parameters.AddWithValue("@IdPrzedmiot", PrzedmiotyDDL.SelectedItem.Value);
                        sqlCmd.Parameters.AddWithValue("@Data", CalendarTextBox.Text);
                        sqlCmd.Parameters.AddWithValue("@GodzinaRozpoczecia", RozpoczecieTextBox.Text + ":00");
                        sqlCmd.Parameters.AddWithValue("@GodzinaZakonczenia", ZakonczenieTextBox.Text + ":00");
                        sqlCmd.Parameters.AddWithValue("@Opis", OpisTextBox.Text);
                        sqlCmd.Parameters.AddWithValue("@IdPracownikProwadzacy", returnPracownikId());
                        sqlCmd.Parameters.AddWithValue("@CzyOgolnoUczelniane", OgolnouczelnianeCheckBox.Checked);

                        if (OgolnouczelnianeCheckBox.Checked == false)
                        {
                            sqlCmd.Parameters.AddWithValue("@IdWydzial", WydzialDDL.SelectedItem.Value);
                            sqlCmd.Parameters.AddWithValue("@IdKierunek", KierunekDDL.SelectedItem.Value);
                        }
                        else if (OgolnouczelnianeCheckBox.Checked)
                        {
                            sqlCmd.Parameters.AddWithValue("@IdWydzial", DBNull.Value);
                            sqlCmd.Parameters.AddWithValue("@IdKierunek", DBNull.Value);
                        }

                        sqlCmd.Parameters.AddWithValue("@CzyZdalnie", ZdalneCheckBox.Checked);
                        sqlCmd.Parameters.AddWithValue("@IdSala", DBNull.Value);

                        sqlCmd.Parameters.AddWithValue("@CzyZastpestwo", ZastepstwoCheckBox.Checked);
                        if (ZastepstwoCheckBox.Checked == false)
                            sqlCmd.Parameters.AddWithValue("@IdPracownikZastepstwo", DBNull.Value);
                        else if (ZastepstwoCheckBox.Checked)
                            sqlCmd.Parameters.AddWithValue("@IdPracownikZastepstwo", ZastepstwoDDL.SelectedItem.Value);

                        sqlCmd.Parameters.AddWithValue("@Temat", TematTextBox.Text);

                        sqlCmd.ExecuteScalar();
                        lblSuccessMessage.Text = "Zaktualizowano rekord.";
                        lblErrorMessage.Text = "";
                        con.Close();
                    }
                }
            }
            else
            {
                lblErrorMessage.Text = "Musisz wypełnić wszystkie pola.";
                lblSuccessMessage.Text = "";
            }
        }


        /// <summary>
        /// Sprawdzenie czy zajęcia są aktywne czy odwołane.
        /// </summary>
        protected int isCancelled()
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();

                // Zapytanie SQL
                string query = @"select CzyOdwolane from dbo.Zajecia WHERE Id = " + Session["resultId"];

                SqlCommand sql = new SqlCommand(query, con);

                int result = Convert.ToInt32(sql.ExecuteScalar());
                con.Close();

                return result;
            }

        }


        /// <summary>
        /// Obsługa odwoływania zajęć.
        /// </summary>
        protected void CancellButton_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();

                // Zapytanie SQL
                string query = @"UPDATE dbo.Zajecia SET CzyOdwolane = 1 WHERE Id = " + Session["resultId"];

                SqlCommand sql = new SqlCommand(query, con);

                _ = sql.ExecuteScalar();
                con.Close();

                lblSuccessMessage.Text = "Zajęcia zostały odwołane.";
                CancellButton.Visible = false;
                RestoreButton.Visible = true;
            }
        }


        /// <summary>
        /// Obsługa przywrócenia aktywności zajęć.
        /// </summary>
        protected void RestoreButton_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();

                // Zapytanie SQL
                string query = @"UPDATE dbo.Zajecia SET CzyOdwolane = 0 WHERE Id = " + Session["resultId"];

                SqlCommand sql = new SqlCommand(query, con);

                _ = sql.ExecuteScalar();
                con.Close();

                lblSuccessMessage.Text = "Zajęcia zostały przywrócone.";
                CancellButton.Visible = true;
                RestoreButton.Visible = false;
            }
        }
    }
}