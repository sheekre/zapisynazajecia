﻿using StronaProjekt;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SqlClient;

namespace StronaProjekt.Tests
{
    [TestClass]
    public class LoginTests
    {
        /// <summary>
        /// Test jednostkowy sprawdzający poprawność łączenia imienia i nazwiska.
        /// </summary>
        [TestMethod()]
        public void ConcatNameAndSurnameTest()
        {
            Classes c = new Classes();

            Assert.AreEqual("Mariusz Mariuszewski", c.ConcatNameAndSurname("Mariusz", "Mariuszewski"));

        }

        /// <summary>
        /// Test jednostkowy sprawdzający pobranie Id kierunku z bazy według podanego oznaczenia.
        /// </summary>
        [TestMethod()]
        public void getIdKierunkiTest()
        {
            ShowDetails c = new ShowDetails();
            Assert.AreEqual(2, c.getIdKierunki("IIZ", @"Data Source=DESKTOP-UOJJ9B7\SQLEXPRESS;Initial Catalog=ZapisyNaZajecia;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"));

            

        }
    }
}
